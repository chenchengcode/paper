const express = require("express");
const router = express.Router();
const { upload } = require("../../util/upload");

router.post("/", upload.single("image"), async (req, res) => {
  // console.log(11);
  req.file.path = req.file.path.replace(/\\/g, "/");
  // console.log(req.file);
  res.json({
    code: 1,
    data: req.file,
    msg: "上传成功",
  });
});

/* router.post("/parts", upload.single("image"), async (req, res) => {
//分片上传就是把每一片存起来，最后前端再发起请求组合分片，new Blob([每一份切片])
  req.file.path = req.file.path.replace(/\\/g, "/");
  console.log(req.file);
  res.json({
    code: 1,
    data: req.file,
    msg: "上传成功",
  });
}); */
module.exports = router;
