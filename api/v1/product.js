const express = require("express");
const router = express.Router();
const { jwtStr } = require("../../config/index.js");
const { Product, User, Category } = require("../../dataBase");
const { getId } = require("../../util/getJwtId");
const { product } = require("../../util/upload");
router.get("/", async (req, res) => {
  let pages = req.query.pages || 1;
  if (pages <= 0) {
    pages = 1;
  }
  let query = {};
  let sort = { _id: -1 };
  if (req.query.goodsName) {
    var name = req.query.goodsName; //获取查询条件
    query.goodsName = { $regex: name }; // 查询条件 正则
  }
  if (req.query.category) {
    // query.category = { $elemMatch: { $eq: req.query.category } };
    query.category = { $in: req.query.category };
  }
  if (req.query.sort == 1) {
    sort = { price: Number(req.query.value) };
  } else {
    sort = { _id: Number(req.query.value)||-1 };
  }
  let ress = await Product.find(query)
    .collation({ locale: "zh", numericOrdering: true })
    .sort(sort)
    .limit(10)
    .skip(10 * (pages - 1));
  let total = await Product.find(query).count();
  res.json({
    data: ress,
    total,
    code: 1,
  });
});
router.post("/add", product.single("product"), async (req, res) => {
  let uid = getId(req);
  let data = req.body;
  data.coverImg = req?.file?.path || req.body.coverImg;
  let user = await User.findById(data.uid || uid);
  data.uid = uid;
  data.userName = user.userName;
  data.avatar = user.avatar;
  console.log(data.category);
  if (!Array.isArray(data.category)) data.category = JSON.parse(data.category);
  let category = await Category.findById(data.category[0]);
  data.firstCategory = category.label;
  // console.log(data,"商品数据");
  let goods = new Product(data);
  let ress = await goods.save();
  res.json({ data: ress, code: 1, msg: `添加${ress.goodsName}成功` });
});

router.get("/mygoods", async (req, res) => {
  let uid = getId(req);
  let pages = req.query.pages;
  // console.log(pages);
  let goods = await Product.find({ uid: uid });
  // .limit(10)
  /* .skip(pages * 10); */
  // console.log(goods);
  res.json({
    data: goods,
    code: 1,
  });
});
router.put("/edit", product.single("product"), async (req, res) => {
  let uid = getId(req);
  let user = await User.findById(uid);
  let role = user.role;
  let file = req.file;
  let updata = req.body;
  let pid = updata.pid;
  updata.category = JSON.parse(updata.category);
  if (file?.path) {
    updata.coverImg = file.path;
  }
  // console.log(updata, file);
  let pro = await Product.findById(pid);
  if (role != "user" || pro.uid == uid) {
    let ress = await Product.findByIdAndUpdate(pid, updata, { new: true });
    res.json({
      data: ress,
      msg: "修改成功",
      code: 1,
    });
  } else {
    res.json({
      msg: "权限不足",
      code: 0,
    });
  }
});
router.get("/ids", async (req, res) => {
  let ids = req.query.ids;
  // console.log(ids);
  let pro = await Product.find({ _id: { $in: ids } });
  res.json({
    data: pro,
    code: 1,
  });
});
router.delete("/del", async (req, res) => {
  let uid = getId(req);
  console.log(req.query.pid);
  let pid = req.body.pid || req.query.pid;
  let user = await User.findById(uid);
  let pro = await Product.findById(pid);
  if (user.role != "user" || pro.uid == uid) {
    let ress = await Product.findByIdAndDelete(pid);
    /* let savee = new Product(ress);
    savee.save(); //数据上传很麻烦 */
    if (ress._id) {
      res.json({
        data: ress,
        msg: "删除成功",
        code: 1,
      });
    }
  } else {
    res.json({
      msg: "权限不足",
      code: 0,
    });
  }
});
router.get("/:id", async (req, res) => {
  let id = req.params.id;
  let ress = await Product.findById(id);
  res.json({
    data: ress,
    code: 1,
  });
});
module.exports = router;
