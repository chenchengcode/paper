const express = require("express");
const router = express.Router();
const { User, Product } = require("../../dataBase");
const { getId } = require("../../util/getJwtId");
router.get("/", async (req, res) => {
  let uid = getId(req);
  if (uid) {
    let user = await User.findById(uid);
    let cart = user.cart;
    // console.log(cart);
    let pro = await Product.find({ _id: { $in: cart } });
    res.json({
      data: pro,
      code: 1,
    });
  }
});
router.get("/:id", async (req, res) => {
  let id = getId(req);
  let pid = req.params.id;
  // console.log(pid);
  if (id) {
    let user = await User.findById(id);
    let cart = user.cart;
    let ress;
    if (cart.indexOf(pid) != -1) {
      let index = cart.findIndex((item) => {
        return item == pid;
      });
      cart.splice(index, 1);
      // console.log(cart, 0);
      ress = await User.findByIdAndUpdate(id, { cart });
      res.json({ data: ress, code: 1, msg: "取消收藏成功", type: 0 });
    } else {
      cart.push(pid);
      // console.log(cart,1);
      ress = await User.findByIdAndUpdate(id, { cart });
      res.json({ data: ress, code: 1, msg: "收藏成功", type: 1 });
    }
  }
});

module.exports = router;
