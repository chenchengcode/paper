const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const { jwtStr } = require("../../config/index.js");
const { User } = require("../../dataBase");
const { getId } = require("../../util/getJwtId");
const multer = require("multer");
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "public/avatar");
  },
  filename: function (req, file, cb) {
    const uniqueSuffix = Date.now();
    let x = file.originalname.split(".");
    let filename = x[x.length - 1]; //上传文件以现在时间命名
    cb(null, uniqueSuffix + "." + filename);
  },
});
const upload = multer({ storage: storage });

router.post("/reg", upload.single("avatar"), async (req, res, next) => {
  //登录接口
  let user = req.body;
  let repeat = await User.findOne({ userName: user.userName });
  if (repeat) {
    res.json({
      code: 0,
      msg: "用户名重复",
    });
  } else {
    let userInfo = new User({
      userName: user.userName,
      passWord: user.passWord,
      gender: user.gender,
      email: user.email,
      phone: user.phone,
      nickName: user.nickName || user.userName,
      avatar: req.file?.path,
    });
    let ress = await userInfo.save();
    delete ress.passWord;
    let token = jwt.sign({ userId: ress._id }, jwtStr, { expiresIn: "15h" });
    res.json({ data: ress, token, code: 1 });
  }
});
router.get("/check", async (req, res) => {
  //检查用户名重复接口
  // let user=req.params
  let repeat = await User.findOne({ userName: req.query.userName });
  if (repeat) {
    res.json({
      code: 0,
      msg: "用户名重复",
    });
  } else {
    res.json({
      code: 1,
      msg: "用户名可用",
    });
  }
});

router.post("/login", async (req, res) => {
  //登录接口
  let userInfo = req.body;
  // console.log(userInfo);
  if (!userInfo.passWord && !userInfo.userName) {
    res.json({
      message: "请输入用户名或密码",
      code: 0,
    });
    return false;
  }
  console.log(req.body);
  try {
    let user = await User.findOne(req.body);
    let token = jwt.sign({ userId: user._id }, jwtStr, { expiresIn: "15d" });
    res.json({ code: 1, data: user, token });
  } catch (err) {
    res.json({
      code: 0,
      err,
      msg: "用户名或密码错误",
    });
  }
});

module.exports = router;
