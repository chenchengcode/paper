const express = require("express");
const router = express.Router();
const { User, Suggest } = require("../../dataBase");
const { getId } = require("../../util/getJwtId");
router.get("/", async (req, res) => {
  /**
   * @param {Boolean} type   是否解决
   * @param {Number} page 页数
   */
  let id = getId(req);
  if (!id) {
    res.json({ msg: "未授权", code: 0 });
    return false;
  }
  let user = await User.findById(id);
  let type = req.query.type;
  let page = req.query.page || 0;
  let ress = null;
  // let total = Math.ceil((await Suggest.find().count()) / 10);
  let total = await Suggest.find().count();
  let query = {};
  if (type == 1) {
    query = {
      state: true,
    };
  } else if (type == 2) {
    query = {
      state: false,
    };
  }
  if (user.role != "user") {
    // query.uid = id;
    ress = await Suggest.find(query)
      .limit(10)
      .skip(page * 10);
    res.json({ data: ress, total: total, code: 1 });
  } else {
    query.uid = id;
    ress = await Suggest.find(query)
      .limit(10)
      .skip(page * 10);
    res.json({ data: ress, total: total, code: 1 });
  }
});
router.put("/add", async (req, res) => {
  let id = getId(req);
  console.log(id, "ID");
  if (id) {
    let info = req.body;
    // console.log(req.body);
    let user = await User.findById(id);
    info.nickName = user.nickName;
    info.avatar = user.avatar;
    info.uid = user._id;
    let xx = new Suggest(info);
    let ress = await xx.save();
    // console.log(ress);
    res.json({
      data: ress,
      code: 1,
      msg: "提交成功",
    });
  }
});
router.post("/reply", async (req, res) => {
  let id = getId(req);
  let user = await User.findById(id);
  if (user.role != "user") {
    let pid = req.body.pid;
    let sugg = await Suggest.findByIdAndUpdate(
      pid,
      { replay: req.body.replay, state: true },
      { new: true }
    );
    console.log(sugg);
    res.json({
      data: sugg,
      code: 1,
      msg: "回复成功",
    });
  } else {
    res.json({
      data: sugg,
      code: 0,
      msg: "权限不足",
    });
  }
});
router.delete("/del", async (req, res) => {
  let id = getId(req);
  let user = await User.findById(id);
  //   console.log(req.body);
  if (user._id == req.body.uid) {
    let ress = await Suggest.findByIdAndDelete(req.body.pid);
    // console.log(ress);
    if (ress) {
      res.json({
        msg: "删除成功",
        code: 1,
        data: ress,
      });
    }
  }
});
module.exports = router;
