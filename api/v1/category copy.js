const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const { jwtStr } = require("../../config/index.js");
const { getId } = require("../../util/getJwtId");
const { Category, User } = require("../../dataBase");
const { gameCategory } = require("../../config/index");
router.get("/", async (req, res) => {
  //   let game = new Category(gameCategory);
  let ress = await Category.find();
  //   console.log(ress);

  res.json({ data: ress });
});
router.get("/add", async (req, res) => {
  let uid = getId(req);
  console.log(uid);
  let user = await User.findById(uid);
  console.log(user.role);
  if (user.role == "user") {
    res.json({
      code: 0,
      msg: "权限不足",
    });
    return;
  }
  let data = JSON.parse(req.query.data);
//   console.log(data);
  let id = req.query._id;
  delete data._id
  console.log(id);
  if (id) {
    let ress = await Category.findByIdAndUpdate({ _id: data._id }, data, {
      new: true,
    });
  } else {
    /* console.log(data.value);
    let has = await Category.find({ value: data.value });
    console.log(has);
    if (has.length > 0) {
      console.log(has);
      res.json({ code: 0, msg: "分类已经存在" });
      return;
    } */
    let categ = new Category(data);
    let ress = await categ.save();
    // console.log(ress);
    if (ress)
      res.json({
        code: 1,
        msg: "添加成功",
      });
  }
});
router.get("/del", async (req, res) => {
  let data = req.query.data;
  //   console.log(data);
  if (data.length == 1) {
    Category.findOneAndDelete({ value: data[0] }).then((ress) => {
      //   console.log(ress);
      if (ress) {
        res.json({
          code: 1,
          msg: "删除成功",
        });
      }
    });
  } else {
    Category.findOne({ value: data[0] }).then((ress) => {
      //   console.log(ress);
      let _id = null;
      if (ress) {
        _id = ress._id;
        let str = "";
        for (let i = 1; i < data.length-1; i++) {
           str+="children["+data[i]+"]."
        }
        console.log(str);   
      }
    });
  }
});
module.exports = router;
