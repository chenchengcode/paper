const express = require("express");
const mongoose = require("mongoose");
const router = express.Router();
const { User, Orders, Product } = require("../../dataBase");
const { getId } = require("../../util/getJwtId");
const { avater } = require("../../util/upload");
const { ObjectId } = mongoose.Types;
router.get("/info", async (req, res) => {
  let id = getId(req);
  console.log(id);
  // console.log(id);
  if (id) {
    let userinfo = await User.findById(id, { passWord: 0 });
    let orders = await Orders.find({ uid: id });
    let goods = await Product.find({ uid: id });
    userinfo.orders = orders;
    userinfo.goods = goods;
    // userinfo.passWord = "";
    res.json({ data: userinfo, code: 1 });
  }
});
router.post("/edit", avater.single("avatar"), async (req, res) => {
  let user = req.query;
  let id = getId(req);
  user.id = id;
  // console.log(user);
  let userInfo = {
    gender: user.gender,
    email: user.email,
    phone: user.phone,
    nickName: user.nickName,
  };
  if (req.file?.path) {
    userInfo.avatar = req.file.path;
  }
  let ress = await User.findByIdAndUpdate({ _id: user.id }, userInfo, {
    new: true,
  });
  // console.log(ress);
  ress.passWord = "******";
  res.json({ code: 1, data: ress });
});
router.get("/", async (req, res) => {
  let id = getId(req);
  // console.log(id);
  /* let query = {
    userName: req.query.key,
  }; */
  let query = [
    { userName: new RegExp(req.query.key, "i") },
    { nickName: new RegExp(req.query.key, "i") },
  ];
  if (ObjectId.isValid(req.query.key)) {
    query.push({
      _id: req.query.key,
    });
  }
  // console.log(req.query.key);
  if (id) {
    let user = await User.findById(id);
    if (user.role == "user") {
      res.json({
        code: 0,
        msg: "未授权",
      });
      return false;
    } else {
      let ress = null;
      if (user.role == "admin") {
        ress = await User.find({
          role: "user",
          $or: query,
        });
      } else if (user.role == "superstar") {
        ress = await User.find({
          role: { $not: /superstar/ },
          $or: query,
        });
      }
      res.json({
        code: 1,
        data: ress,
      });
    }
  }
});
router.put("/editad", avater.single("avatar"), async (req, res) => {
  let user = req.body;
  let file = req.file;
  let id = getId(req);
  let editID = user.editID;
  delete user.editID;
  let auth = await User.findById(id);
  // console.log(editID);
  if (auth.role != "user") {
    if (auth.role != "superstar") {
      delete user.role;
    }
    // console.log(user);
    if (file?.path) {
      user.avatar = file?.path;
    }
    let ress = await User.findByIdAndUpdate(editID, user, { new: true });
    res.json({
      msg: "修改成功",
      data: ress,
      code: 1,
    });
  } else {
    res.json({
      msg: "权限不足",
      code: 0,
    });
  }
});
router.delete("/del", async (req, res) => {
  let id = getId(req);
  let uid = req.body.uid;
  // console.log(uid);
  let user = await User.findById(id);
  if (user.role != "user") {
    let ress = await User.findByIdAndDelete(uid);
    res.json({
      msg: "删除成功",
      data: ress,
      code: 1,
    });
  } else {
    res.json({
      msg: "权限不足",
      code: 0,
    });
  }
});
router.post("/pass", async (req, res) => {
  let id = getId(req);
  let { passWord, newPass } = req.body;
  // console.log(passWord, newPass);
  let user = await User.findOneAndUpdate(
    { _id: id, passWord: passWord },
    { passWord: newPass }
  );
  if (user) {
    res.json({
      code: 1,
      msg: "修改成功，请重新登录",
    });
  } else {
    res.json({
      code: 0,
      msg: "密码错误",
    });
  }
});
module.exports = router;
