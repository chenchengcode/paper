const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const { jwtStr } = require("../../config/index.js");
const { getId } = require("../../util/getJwtId");
const { Category, User } = require("../../dataBase");
const { gameCategory } = require("../../config/index");
const { array } = require("../../util/upload.js");
router.get("/", async (req, res) => {
  let ress = await Category.find({ deep: 1 });
  res.json({ data: ress });
});
router.get("/add", async (req, res) => {
  let admin = getId(req);
  if (admin.role == "user") {
    res.json({
      code: 0,
      msg: "权限不足!!!",
    });
    return false;
  }
  let data = req.query;
  let id = data.id;
  delete data.id;
  let has = null;
  let fath = null;
  if (id) {
    fath = await Category.findById(id);
    data.deep = fath.deep + 1;
  } else {
    data.deep = 1;
    let cat = new Category(data);
    let result = await cat.save();
    res.json({
      code: 1,
      msg: "添加成功",
      data: result,
    });
    return 0;
    data.deep = 1;
  }
  let cat = new Category(data);
  let result = await cat.save();
  fath.children.push(result._id);
  let updata = await Category.findByIdAndUpdate(id, fath);
  res.json({
    code: 1,
    msg: "添加分类成功",
    data: result,
  });
});
router.delete("/:id", async (req, res) => {
  let id = req.params.id;
  /* let del = async (id) => {
    let current = await Category.findById(id);
    if (current.children.length > 0) {
      current.children.forEach(async (item) => {
        del(item);
      });
    } else {
      await Category.findByIdAndDelete(id);
    }
  }; */
  let ress = await Category.findByIdAndDelete(id);
  // let ress = await del(id);
  if (ress) {
    res.json({ data: ress, code: 1, msg: `删除${ress.label}分类成功` });
  } else {
    res.json({ code: 0, msg: "删除失败" });
  }
});
router.get("/:id", async (req, res) => {
  let id = req.params.id;
  let ress = await Category.findById(id);
  let arr = ress.children;
  /* let arr1 = arr.map((item) => Category.findById(arr[i].toString()));
  const allData = await Promise.all(arr1); */
  //异步解决简化版

  new Promise(async (resolve, reject) => {
    let result = [];
    let i = 0;
    while (i < arr.length) {
      await Category.findById(arr[i].toString()).then((res) => {
        result.push(res);
        i++;
      });
    }
    if (i == arr.length) {
      resolve(result);
    }
  }).then((ress) => {
    ress = ress.filter((item) => {
      if (item != null) return item;
    });
    res.json({ data: ress });
  });
});
router.get("/updata/:id", async (req, res) => {
  let id = req.params.id;
  let value = req.query;
  let ress = await Category.findByIdAndUpdate(id, value);
  // console.log(ress);
  res.json({
    // data: ress,
    code: 1,
    msg: `修改${ress.label}分类为${value.label}分类成功`,
  });
});
module.exports = router;
