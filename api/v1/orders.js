const { query } = require("express");
const express = require("express");
const router = express.Router();
const { User, Orders, Product } = require("../../dataBase");
const { getId } = require("../../util/getJwtId");
router.get("/", async (req, res) => {
  let id = getId(req);
  let query = {};
  if (req.query.key) {
    query = {
      goodsName: new RegExp(req.query.key, "i"),
    };
  }
  let ress = await Orders.find({ uid: id, ...query }, null, {
    sort: { state: 1 },
  });
  res.json({
    data: ress,
    code: 1,
  });
});
/* router.get("/my", async (req, res) => {
  let id = getId(req);
  let query = {};
  if (req.query.key) {
    query = {
      goodsName: new RegExp(req.query.key, "i"),
    };
  }
  let ress = await Orders.find({ uid: id, ...query }, null, {
    sort: { state: 1 },
  });
  res.json({
    data: ress,
    code: 1,
  });
}); */
router.get("/add", async (req, res) => {
  let id = getId(req);
  // console.log(id);
  let user = await User.findById(id);
  let temp = req.query.temp;
  let pro = await Product.findById(req.query.pid);
  let ower = await User.findById(pro.uid);
  // console.log(pro, temp);
  let data = new Orders({
    temp,
    uid: id,
    ower: JSON.stringify({
      nick: ower.nickName,
      avatar: ower.avatar,
      id: ower._id,
    }),
    goodsName: pro.goodsName,
    price: pro.price,
    pid: pro._id,
    buyName: user.nickName,
  });
  let ress = await data.save();
  res.json({
    data: ress,
    code: 1,
    msg: "订单生成成功，等待审核",
  });
});
router.post("/confirm", async (req, res) => {
  let id = getId(req);
  let oid = req.body.oid;
  let user = await User.findById(id);
  let order = await Orders.findById(oid);

  if (user.role != "user") {
    let ress = null;
    if (order.state == 1) {
      ress = await Orders.findByIdAndUpdate(oid, { state: 2 }, { new: true });
      res.json({
        data: ress,
        code: 1,
        msg: "确定成功",
      });
    } else {
      ress = await Orders.findByIdAndUpdate(oid, { state: 1 }, { new: true });
      res.json({
        data: ress,
        code: 1,
        msg: "修改为未支付",
      });
    }
  }
});
router.get("/wait", async (req, res) => {
  let id = getId(req);
  // let user = await User.findById(id);
  let ress = await Orders.find({ uid: id, state: 2 });
  // console.log(ress);
  res.json({
    data: ress,
    code: 1,
  });
});
router.post("/change", async (req, res) => {
  let id = getId(req);
  let oid = req.body.oid;
  let user = await User.findById(id);
  let order = await Orders.findById(oid);
  let ress = null;
  if (user._id != order.uid) {
    return false;
  }
  if (order.state == 2) {
    ress = await Orders.findByIdAndUpdate(oid, { state: 3 }, { new: true });
    res.json({
      data: ress,
      code: 1,
      msg: "确定发货成功",
    });
  } else {
    ress = await Orders.findByIdAndUpdate(oid, { state: 2 }, { new: true });
    res.json({
      data: ress,
      code: 1,
      msg: "取消发货",
    });
  }
});
router.post("/shouhuo", async (req, res) => {
  let id = getId(req);
  let oid = req.body.oid;
  let user = await User.findById(id);
  let order = await Orders.findById(oid);
  let ress = null;
  if (user._id != order.uid) {
    return false;
  }
  if (order.state == 3) {
    ress = await Orders.findByIdAndUpdate(oid, { state: 4 }, { new: true });
    res.json({
      data: ress,
      code: 1,
      msg: "确定收货成功",
    });
  } else {
    ress = await Orders.findByIdAndUpdate(oid, { state: 3 }, { new: true });
    res.json({
      data: ress,
      code: 1,
      msg: "取消确定收货",
    });
  }
});
module.exports = router;
