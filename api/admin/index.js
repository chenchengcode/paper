const { User } = require("../../dataBase");
const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const { upload } = require("../../util/upload");
const { jwtStr } = require("../../config/index.js");
router.post("/login", async (req, res) => {
  //登录接口
  console.log(req.body);
  if (!req.body.userName || !req.body.userName) {
    return res.json({
      code: 0,
      msg: "用户名或密码错误",
    });
  }
  let user = await User.findOne(req.body);
  // console.log(user);
  if (user?.role != "user" && user) {
    let token = jwt.sign({ userId: user._id }, jwtStr, { expiresIn: "15d" });
    res.json({ code: 1, token });
  } else {
    res.json({
      code: 0,
      msg: "用户名或密码错误",
    });
  }
});
router.post("/upload", upload.single("image"), async (req, res) => {
  //登录接口
  // console.log(req.body);
  console.log(req.file);
  res.json({
    code: 1,
    data: req.file,
    msg: "上传成功",
  });
});
module.exports = router;
