const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const ip = require("ip");
const fs = require("fs");
const cookieParser = require("cookie-parser");
// const os = require("os");
var logger = require("morgan");
const { getId } = require("./util/getJwtId");
const app = express();
app.use(cookieParser());
app.use("/public", express.static("./public"));
app.use("/static", express.static("./static"));
// app.use(logger("dev"));
app.use(cors());
// 解决跨域问题
app.all("/api/v1/product/del", (req, res, next) => {
    let id = getId(req);
    if (!id) {
        res.json({
            msg: "请先登录",
            status: 403,
            data: null,
        });
    }
    console.log(req.body, req.params, req.query, "+++", id);
    next();
});
app.use(
    bodyParser.urlencoded({
        extended: false,
        limit: "10000kb",
    })
);
// parse application/json
app.use(
    bodyParser.json({
        limit: "10000kb",
    })
);

app.get("/upload", (req, res) => {
    let text = fs.readFileSync("./upload.html").toString();
    // console.log(text);
    // res.send(text);
    // res.sendFile(__dirname+"/upload.html");
    res.send(text);
});

app.use("/api/v1/auth", require("./api/v1/auth"));
app.use("/api/v1/users", require("./api/v1/users"));
app.use("/api/v1/category", require("./api/v1/category"));
app.use("/api/v1/product", require("./api/v1/product"));
app.use("/api/v1/cart", require("./api/v1/cart"));
app.use("/api/v1/suggest", require("./api/v1/suggest"));
app.use("/api/v1/orders", require("./api/v1/orders"));
app.use("/api/admin", require("./api/admin/index"));
app.use("/api/v1/upload", require("./api/v1/upload"));
let port = 3009;
process.on("uncaughtException", function (err) {
    console.log(JSON.stringify(err.stack), "err.stack");
    if (err.stack.indexOf("address already in use") != -1) {
        port = port + 1;
        app.listen(port, () => {
            console.log(
                `服务运行在\n    -Local:    http://127.0.0.1:${port}\n  -Network:    http://${ip.address()}:${port}/`
            );
        });
    }
});
// app.listen(3009, ip.address());
app.get("/favicon.ico", (req, res) => {
    return false;
});
app.listen(port, () => {
    console.log(
        `服务运行在\n    -Local:    http://127.0.0.1:3009\n  -Network:    http://${ip.address()}:3009/`
    );
});
app.get("/", (req, res) => {
    res.json({
        code: 1,
        msg: "服务器运作正常",
        ip: ip.address() + port,
        local: "127.0.0.1:" + port,
    });
});
