const mongoose = require("mongoose");
const userSchema = new mongoose.Schema({
  userName: {
    type: String,
    require: true,
  },
  nickName: {
    type: String,
  },
  phone: {
    type: String,
    // default: 15512369658,
  },
  passWord: {
    type: String,
    require: true,
    // select: false,
  },
  state: {
    type: Boolean,
    default: true,
  },
  orders: {
    type: Array,
    default: [],
  },
  goods: {
    type: Array,
    default: [],
  },
  createDate: {
    type: Date,
    default: Date.now(),
  },
  role: {
    type: String,
    default: "user",
  },
  gender: {
    type: String,
    default: 3,
  },
  email: {
    type: String,
  },
  avatar: {
    type: String,
    default: "/public/avatar/avatar.jpg",
  },
  cart: {
    type: Array,
    default: [],
  },
});
module.exports = mongoose.model("User", userSchema);
