const mongoose = require("mongoose");
const User = require("./user");
const Product = require("./products");
const Category = require("./category");
const Suggest = require("./suggest");
const Orders = require("./orders");
// const Cart = require("./cart");
mongoose
  .connect("mongodb://localhost:27017/trade", {
    useNewUrlParser: true,
  })
  .then((res) => {
    console.log("数据库连接成功");
  })
  .catch((err) => {
    console.log(err);
  });
module.exports = {
  User,
  Product,
  Category,
  Suggest,
  Orders,
};
