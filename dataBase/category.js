const mongoose = require("mongoose");
const categorySchema = new mongoose.Schema({
  value: {
    type: String,
    require: true,
  },
  label: {
    type: String,
    require: true,
  },
  children: {
    type: Array,
  },
  deep: {
    required: true,
    type: Number,
  },
});
module.exports = mongoose.model("category", categorySchema);
