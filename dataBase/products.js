const mongoose = require("mongoose");
const productSchema = new mongoose.Schema({
  goodsName: {
    type: String,
    require: true,
  },
  context: {
    type: String,
  },
  price: {
    type: String,
    require: true,
  },
  createDate: {
    type: Date,
    default: Date.now(),
  },
  uid: {
    type: String,
    required: true,
  },
  userName: {
    type: String,
    required: true,
  },
  avatar: {
    type: String,
    required: true,
  },
  category: {
    type: Array,
    require: true,
  },
  firstCategory: {
    type: String,
  },
  coverImg: {
    type: Array,
    require: true,
  },
  des: {
    type: String,
    require: true,
    default: "",
  },
  describeImg: {
    type: String,
    require: true,
  },
  state: {
    type: Boolean,
    require: true,
    default: 1,
  },
  buy: {
    type: Array,
  },
});
module.exports = mongoose.model("product", productSchema);
