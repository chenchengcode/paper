const mongoose = require("mongoose");
const categorySchema = new mongoose.Schema({
  temp: {
    type: String,
    require: true,
  },
  pid: {
    type: String,
    require: true,
  },
  buyName: {
    type: String,
  },
  create: {
    type: Date,
    default: Date.now(),
  },
  uid: {
    type: String,
    require: true,
  },
  ower: {
    type: String,
    required: true,
  },
  goodsName: {
    type: String,
    required: true,
  },
  price: {
    type: Number,
  },
  payId: {
    type: String,
  },
  state: {
    type: String,
    default: 1,
  },
});
module.exports = mongoose.model("order", categorySchema);
