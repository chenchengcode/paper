const mongoose = require("mongoose");
const productSchema = new mongoose.Schema({
  nickName: {
    type: String,
    require: true,
  },
  uid: {
    type: String,
    require: true,
  },
  create: {
    type: Date,
    default: Date.now(),
  },
  avatar: {
    type: String,
    required: true,
  },
  text: {
    type: String,
    required: true,
  },
  title: {
    type: String,
  },
  replay: {
    type: String,
  },
  state: {
    type: Boolean,
    require: true,
    default: false,
  },
});
module.exports = mongoose.model("suggest", productSchema);
