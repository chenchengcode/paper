# game_trade

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
node app
```

### 最好使用进程守护运行（有较多bug可能会导致服务挂掉）

```
npm i forever -g
forever start app.js
```

