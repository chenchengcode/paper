const multer = require("multer");
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "public/avatar");
  },
  filename: function (req, file, cb) {
    const uniqueSuffix = Date.now();
    let x = file.originalname.split(".");
    let filename = x[x.length - 1]; //上传文件以现在时间命名
    cb(null, uniqueSuffix + "." + filename);
  },
});
const avater = multer({ storage: storage });
const storage1 = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "public/product");
  },
  filename: function (req, file, cb) {
    const uniqueSuffix = Date.now();
    let x = file.originalname.split(".");
    let filename = x[x.length - 1]; //上传文件以现在时间命名
    cb(null, uniqueSuffix + "." + filename);
  },
});
const product = multer({ storage: storage1 });
//单独上传图片接口
const storage2 = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "public/upload");
  },
  filename: function (req, file, cb) {
    const uniqueSuffix = Date.now();
    let x = file.originalname.split(".");
    let filename = x[x.length - 1]; //上传文件以现在时间命名
    cb(null, "upload" + uniqueSuffix + "." + filename);
  },
});
const upload = multer({ storage: storage2 });
module.exports = { avater, product, upload };
