const jwt = require("jsonwebtoken");
const { jwtStr } = require("../config");
function getId(req) {
  // console.log(req.headers);
  const cookie = req.headers.authorization?.split("="); // 获取token
  // console.log(cookie);
  if (!cookie[1]) return false;
  let token = cookie[1];
  // console.log(cookie,req.headers.authorization);
  // console.log(token, typeof token, Boolean(token));
  if (token != "null") {
    try {
      const decoded = jwt.verify(token, jwtStr);
      const { userId } = decoded;
      return userId;
    } catch {
      return false;
    }
  } else {
    return false;
  }
}
module.exports = {
  getId,
};
