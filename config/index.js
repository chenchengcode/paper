const jwtStr = "chencheng";
let gameCategory = {
    value: "CS:GO",
    label: "CS:GO",
    categories: 3,
    children: [
      {
        value: "dagger",
        label: "匕首",
        children: [
          {
            value: "butterflyKnife",
            label: "蝴蝶刀",
          },
          {
            value: "machetes",
            label: "弯刀",
          },
          {
            value: "foldingKnife",
            label: "折叠刀",
          },
        ],
      },
      {
        value: "pistol",
        label: "手枪",
      },
    ],
  };
module.exports = {jwtStr,gameCategory};
